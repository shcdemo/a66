<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66333">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King and Queen, a proclamation requiring the attendance of the members of both Houses of Parliament</title>
    <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66333 of text R38112 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing W2627). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66333</idno>
    <idno type="STC">Wing W2627</idno>
    <idno type="STC">ESTC R38112</idno>
    <idno type="EEBO-CITATION">17191978</idno>
    <idno type="OCLC">ocm 17191978</idno>
    <idno type="VID">106140</idno>
    <idno type="PROQUESTGOID">2240858207</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66333)</note>
    <note>Transcribed from: (Early English Books Online ; image set 106140)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1624:55)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King and Queen, a proclamation requiring the attendance of the members of both Houses of Parliament</title>
      <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
      <author>Mary II, Queen of England, 1662-1694.</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Charles Bill, and the executrix of Thomas Newcomb deceas'd ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1694.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall, the fourth day of October, 1694. In the sixth year of our reign."</note>
      <note>Reproduction of original in the Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament.</term>
     <term>Great Britain -- Politics and government -- 1689-1702.</term>
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King and Queen, a proclamation, requiring the attendance of the members of both Houses of Parliament.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1694</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>304</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2008-02</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-03</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-04</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-04</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66333-t">
  <body xml:id="A66333-e0">
   <div type="royal_proclamation" xml:id="A66333-e10">
    <pb facs="tcp:106140:1" xml:id="A66333-001-a"/>
    <head xml:id="A66333-e20">
     <figure xml:id="A66333-e30">
      <figure xml:id="A66333-e40">
       <figDesc xml:id="A66333-e50">monogram of 'W' (William) superimposed on' M' (Mary)</figDesc>
      </figure>
      <p xml:id="A66333-e60">
       <w lemma="diev" pos="ffr" xml:id="A66333-001-a-0010">DIEV</w>
       <w lemma="et" pos="ffr" xml:id="A66333-001-a-0020">ET</w>
       <w lemma="mon" pos="ffr" xml:id="A66333-001-a-0030">MON</w>
       <w lemma="droit" pos="nn1" xml:id="A66333-001-a-0040">DROIT</w>
      </p>
      <p xml:id="A66333-e70">
       <w lemma="honi" pos="ffr" xml:id="A66333-001-a-0050">HONI</w>
       <w lemma="soit" pos="ffr" xml:id="A66333-001-a-0060">SOIT</w>
       <w lemma="qvi" pos="ffr" xml:id="A66333-001-a-0070">QVI</w>
       <w lemma="mal" pos="ffr" xml:id="A66333-001-a-0080">MAL</w>
       <w lemma="y" pos="ffr" xml:id="A66333-001-a-0090">Y</w>
       <w lemma="pense" pos="ffr" xml:id="A66333-001-a-0100">PENSE</w>
      </p>
      <figDesc xml:id="A66333-e80">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="A66333-e90">
     <w lemma="by" pos="acp" xml:id="A66333-001-a-0110">By</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0120">the</w>
     <w lemma="king" pos="n1" xml:id="A66333-001-a-0130">King</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-0140">and</w>
     <w lemma="queen" pos="n1" xml:id="A66333-001-a-0150">Queen</w>
     <pc xml:id="A66333-001-a-0160">,</pc>
     <w lemma="a" pos="d" xml:id="A66333-001-a-0170">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A66333-001-a-0180">PROCLAMATION</w>
     <pc xml:id="A66333-001-a-0190">,</pc>
     <w lemma="require" pos="vvg" xml:id="A66333-001-a-0200">Requiring</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0210">the</w>
     <w lemma="attendance" pos="n1" xml:id="A66333-001-a-0220">Attendance</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0230">of</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0240">the</w>
     <w lemma="member" pos="n2" xml:id="A66333-001-a-0250">Members</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0260">of</w>
     <w lemma="both" pos="d" xml:id="A66333-001-a-0270">both</w>
     <w lemma="house" pos="n2" xml:id="A66333-001-a-0280">Houses</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0290">of</w>
     <w lemma="parliament" pos="n1" xml:id="A66333-001-a-0300">PARLIAMENT</w>
     <pc unit="sentence" xml:id="A66333-001-a-0310">.</pc>
    </head>
    <opener xml:id="A66333-e100">
     <signed xml:id="A66333-e110">
      <w lemma="Marie" pos="nn1" xml:id="A66333-001-a-0320">Marie</w>
      <w lemma="r." pos="ab" xml:id="A66333-001-a-0330">R.</w>
      <pc unit="sentence" xml:id="A66333-001-a-0340"/>
     </signed>
    </opener>
    <p xml:id="A66333-e120">
     <w lemma="we" pos="pns" rend="decorinit" xml:id="A66333-001-a-0350">WE</w>
     <w lemma="be" pos="vvg" xml:id="A66333-001-a-0360">being</w>
     <w lemma="desirous" pos="j" xml:id="A66333-001-a-0370">desirous</w>
     <w lemma="that" pos="cs" xml:id="A66333-001-a-0380">that</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0390">the</w>
     <w lemma="member" pos="n2" xml:id="A66333-001-a-0400">Members</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0410">of</w>
     <w lemma="both" pos="d" xml:id="A66333-001-a-0420">both</w>
     <w lemma="house" pos="n2" xml:id="A66333-001-a-0430">Houses</w>
     <w lemma="may" pos="vmb" xml:id="A66333-001-a-0440">may</w>
     <w lemma="have" pos="vvi" xml:id="A66333-001-a-0450">have</w>
     <w lemma="convenient" pos="j" xml:id="A66333-001-a-0460">convenient</w>
     <w lemma="notice" pos="n1" xml:id="A66333-001-a-0470">Notice</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0480">of</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0490">the</w>
     <w lemma="time" pos="n1" xml:id="A66333-001-a-0500">time</w>
     <w lemma="when" pos="crq" xml:id="A66333-001-a-0510">when</w>
     <w lemma="their" pos="po" xml:id="A66333-001-a-0520">their</w>
     <w lemma="attendance" pos="n1" xml:id="A66333-001-a-0530">Attendance</w>
     <w lemma="in" pos="acp" xml:id="A66333-001-a-0540">in</w>
     <w lemma="parliament" pos="n1" xml:id="A66333-001-a-0550">Parliament</w>
     <w lemma="will" pos="vmb" xml:id="A66333-001-a-0560">will</w>
     <w lemma="be" pos="vvi" xml:id="A66333-001-a-0570">be</w>
     <w lemma="requisite" pos="j" xml:id="A66333-001-a-0580">Requisite</w>
     <pc xml:id="A66333-001-a-0590">,</pc>
     <w lemma="to" pos="prt" xml:id="A66333-001-a-0600">to</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0610">the</w>
     <w lemma="end" pos="n1" xml:id="A66333-001-a-0620">end</w>
     <w lemma="they" pos="pns" xml:id="A66333-001-a-0630">they</w>
     <w lemma="may" pos="vmb" xml:id="A66333-001-a-0640">may</w>
     <w lemma="order" pos="vvi" xml:id="A66333-001-a-0650">order</w>
     <w lemma="their" pos="po" xml:id="A66333-001-a-0660">their</w>
     <w lemma="affair" pos="n2" xml:id="A66333-001-a-0670">Affairs</w>
     <w lemma="so" pos="av" xml:id="A66333-001-a-0680">so</w>
     <w lemma="as" pos="acp" xml:id="A66333-001-a-0690">as</w>
     <w lemma="that" pos="cs" xml:id="A66333-001-a-0700">that</w>
     <w lemma="there" pos="av" xml:id="A66333-001-a-0710">there</w>
     <w lemma="then" pos="av" xml:id="A66333-001-a-0720">then</w>
     <w lemma="be" pos="vvi" xml:id="A66333-001-a-0730">be</w>
     <w lemma="a" pos="d" xml:id="A66333-001-a-0740">a</w>
     <w lemma="full" pos="j" xml:id="A66333-001-a-0750">full</w>
     <w lemma="assembly" pos="n1" xml:id="A66333-001-a-0760">Assembly</w>
     <pc xml:id="A66333-001-a-0770">,</pc>
     <w lemma="have" pos="vvb" xml:id="A66333-001-a-0780">Have</w>
     <pc join="right" xml:id="A66333-001-a-0790">(</pc>
     <w lemma="with" pos="acp" xml:id="A66333-001-a-0800">with</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-0810">the</w>
     <w lemma="advice" pos="n1" xml:id="A66333-001-a-0820">Advice</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-0830">of</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-0840">Our</w>
     <w lemma="privy" pos="j" xml:id="A66333-001-a-0850">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66333-001-a-0860">Council</w>
     <pc xml:id="A66333-001-a-0870">)</pc>
     <w lemma="think" pos="vvd" xml:id="A66333-001-a-0880">thought</w>
     <w lemma="fit" pos="j" xml:id="A66333-001-a-0890">fit</w>
     <w lemma="to" pos="prt" xml:id="A66333-001-a-0900">to</w>
     <w lemma="issue" pos="vvi" xml:id="A66333-001-a-0910">Issue</w>
     <w lemma="this" pos="d" xml:id="A66333-001-a-0920">this</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-0930">Our</w>
     <w lemma="royal" pos="j" xml:id="A66333-001-a-0940">Royal</w>
     <w lemma="proclamation" pos="n1" xml:id="A66333-001-a-0950">Proclamation</w>
     <pc xml:id="A66333-001-a-0960">,</pc>
     <w lemma="hereby" pos="av" xml:id="A66333-001-a-0970">Hereby</w>
     <w lemma="declare" pos="vvg" xml:id="A66333-001-a-0980">Declaring</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-0990">and</w>
     <w lemma="publish" pos="vvg" xml:id="A66333-001-a-1000">Publishing</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-1010">Our</w>
     <w lemma="will" pos="n1" xml:id="A66333-001-a-1020">Will</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-1030">and</w>
     <w lemma="pleasure" pos="n1" xml:id="A66333-001-a-1040">Pleasure</w>
     <pc xml:id="A66333-001-a-1050">,</pc>
     <w lemma="that" pos="cs" xml:id="A66333-001-a-1060">That</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-1070">Our</w>
     <w lemma="say" pos="j-vn" xml:id="A66333-001-a-1080">said</w>
     <w lemma="parliament" pos="n1" xml:id="A66333-001-a-1090">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A66333-001-a-1100">shall</w>
     <pc xml:id="A66333-001-a-1110">,</pc>
     <w lemma="on" pos="acp" xml:id="A66333-001-a-1120">on</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1130">the</w>
     <w lemma="twenty" pos="crd" xml:id="A66333-001-a-1140">Twenty</w>
     <w lemma="five" pos="ord" xml:id="A66333-001-a-1150">fifth</w>
     <w lemma="day" pos="n1" xml:id="A66333-001-a-1160">day</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1170">of</w>
     <hi xml:id="A66333-e130">
      <w lemma="October" pos="nn1" xml:id="A66333-001-a-1180">October</w>
     </hi>
     <w lemma="instant" pos="j" xml:id="A66333-001-a-1190">Instant</w>
     <pc xml:id="A66333-001-a-1200">,</pc>
     <w lemma="to" pos="acp" xml:id="A66333-001-a-1210">to</w>
     <w lemma="which" pos="crq" xml:id="A66333-001-a-1220">which</w>
     <w lemma="day" pos="n1" xml:id="A66333-001-a-1230">day</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1240">the</w>
     <w lemma="same" pos="d" xml:id="A66333-001-a-1250">same</w>
     <w lemma="be" pos="vvz" xml:id="A66333-001-a-1260">is</w>
     <w lemma="now" pos="av" xml:id="A66333-001-a-1270">now</w>
     <w lemma="prorogue" pos="vvn" xml:id="A66333-001-a-1280">Prorogued</w>
     <pc xml:id="A66333-001-a-1290">,</pc>
     <w lemma="be" pos="vvb" xml:id="A66333-001-a-1300">be</w>
     <w lemma="further" pos="avc-j" xml:id="A66333-001-a-1310">further</w>
     <w lemma="prorogue" pos="vvn" xml:id="A66333-001-a-1320">Prorogued</w>
     <w lemma="unto" pos="acp" xml:id="A66333-001-a-1330">unto</w>
     <hi xml:id="A66333-e140">
      <w lemma="Tuesday" pos="nn1" xml:id="A66333-001-a-1340">Tuesday</w>
     </hi>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1350">the</w>
     <w lemma="six" pos="ord" xml:id="A66333-001-a-1360">Sixth</w>
     <w lemma="day" pos="n1" xml:id="A66333-001-a-1370">day</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1380">of</w>
     <hi xml:id="A66333-e150">
      <w lemma="November" pos="nn1" xml:id="A66333-001-a-1390">November</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A66333-001-a-1400">next</w>
     <pc xml:id="A66333-001-a-1410">;</pc>
     <w lemma="in" pos="acp" xml:id="A66333-001-a-1420">In</w>
     <w lemma="order" pos="n1" xml:id="A66333-001-a-1430">Order</w>
     <w lemma="to" pos="acp" xml:id="A66333-001-a-1440">to</w>
     <w lemma="which" pos="crq" xml:id="A66333-001-a-1450">which</w>
     <w lemma="prorogation" pos="n1" xml:id="A66333-001-a-1460">Prorogation</w>
     <pc xml:id="A66333-001-a-1470">,</pc>
     <w lemma="we" pos="pns" xml:id="A66333-001-a-1480">We</w>
     <w lemma="shall" pos="vmb" xml:id="A66333-001-a-1490">shall</w>
     <w lemma="expect" pos="vvi" xml:id="A66333-001-a-1500">expect</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1510">the</w>
     <w lemma="attendance" pos="n1" xml:id="A66333-001-a-1520">Attendance</w>
     <w lemma="only" pos="av-j" xml:id="A66333-001-a-1530">only</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1540">of</w>
     <w lemma="such" pos="d" xml:id="A66333-001-a-1550">such</w>
     <w lemma="member" pos="n2" xml:id="A66333-001-a-1560">Members</w>
     <w lemma="as" pos="acp" xml:id="A66333-001-a-1570">as</w>
     <w lemma="shall" pos="vmb" xml:id="A66333-001-a-1580">shall</w>
     <w lemma="be" pos="vvi" xml:id="A66333-001-a-1590">be</w>
     <w lemma="resident" pos="nn1" xml:id="A66333-001-a-1600">Resident</w>
     <w lemma="in" pos="acp" xml:id="A66333-001-a-1610">in</w>
     <w lemma="or" pos="cc" xml:id="A66333-001-a-1620">or</w>
     <w lemma="near" pos="acp" xml:id="A66333-001-a-1630">near</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-1640">Our</w>
     <w lemma="city" pos="n2" xml:id="A66333-001-a-1650">Cities</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1660">of</w>
     <hi xml:id="A66333-e160">
      <w lemma="London" pos="nn1" xml:id="A66333-001-a-1670">London</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-1680">and</w>
     <hi xml:id="A66333-e170">
      <w lemma="Westminster" pos="nn1" xml:id="A66333-001-a-1690">Westminster</w>
      <pc xml:id="A66333-001-a-1700">:</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-1710">And</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-1720">Our</w>
     <w lemma="purpose" pos="n1" xml:id="A66333-001-a-1730">purpose</w>
     <w lemma="be" pos="vvg" xml:id="A66333-001-a-1740">being</w>
     <w lemma="that" pos="cs" xml:id="A66333-001-a-1750">that</w>
     <w lemma="our" pos="po" xml:id="A66333-001-a-1760">Our</w>
     <w lemma="say" pos="j-vn" xml:id="A66333-001-a-1770">said</w>
     <w lemma="house" pos="n2" xml:id="A66333-001-a-1780">Houses</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1790">of</w>
     <w lemma="parliament" pos="n1" xml:id="A66333-001-a-1800">Parliament</w>
     <w lemma="shall" pos="vmb" xml:id="A66333-001-a-1810">shall</w>
     <w lemma="not" pos="xx" xml:id="A66333-001-a-1820">not</w>
     <w lemma="only" pos="av-j" xml:id="A66333-001-a-1830">only</w>
     <w lemma="meet" pos="vvi" xml:id="A66333-001-a-1840">Meet</w>
     <w lemma="upon" pos="acp" xml:id="A66333-001-a-1850">upon</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1860">the</w>
     <w lemma="say" pos="vvd" xml:id="A66333-001-a-1870">said</w>
     <w lemma="six" pos="ord" xml:id="A66333-001-a-1880">Sixth</w>
     <w lemma="day" pos="n1" xml:id="A66333-001-a-1890">day</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-1900">of</w>
     <hi xml:id="A66333-e180">
      <w lemma="November" pos="nn1" xml:id="A66333-001-a-1910">November</w>
     </hi>
     <w lemma="next" pos="ord" xml:id="A66333-001-a-1920">next</w>
     <pc xml:id="A66333-001-a-1930">,</pc>
     <w lemma="but" pos="acp" xml:id="A66333-001-a-1940">but</w>
     <w lemma="shall" pos="vmb" xml:id="A66333-001-a-1950">shall</w>
     <w lemma="sit" pos="vvi" reg="Sat" xml:id="A66333-001-a-1960">Sit</w>
     <w lemma="for" pos="acp" xml:id="A66333-001-a-1970">for</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-1980">the</w>
     <w lemma="dispatch" pos="vvb" xml:id="A66333-001-a-1990">Dispatch</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-2000">of</w>
     <w lemma="divers" pos="j" xml:id="A66333-001-a-2010">divers</w>
     <w lemma="weighty" pos="j" xml:id="A66333-001-a-2020">Weighty</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-2030">and</w>
     <w lemma="important" pos="j" xml:id="A66333-001-a-2040">Important</w>
     <w lemma="affair" pos="n2" xml:id="A66333-001-a-2050">Affairs</w>
     <pc xml:id="A66333-001-a-2060">,</pc>
     <w lemma="we" pos="pns" xml:id="A66333-001-a-2070">We</w>
     <w lemma="do" pos="vvb" xml:id="A66333-001-a-2080">do</w>
     <w lemma="therefore" pos="av" xml:id="A66333-001-a-2090">therefore</w>
     <w lemma="hereby" pos="av" xml:id="A66333-001-a-2100">hereby</w>
     <w lemma="charge" pos="n1" xml:id="A66333-001-a-2110">Charge</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-2120">and</w>
     <w lemma="require" pos="vvi" xml:id="A66333-001-a-2130">Require</w>
     <w lemma="all" pos="d" xml:id="A66333-001-a-2140">all</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-2150">the</w>
     <w lemma="lord" pos="n2" xml:id="A66333-001-a-2160">Lords</w>
     <w lemma="spiritual" pos="j" xml:id="A66333-001-a-2170">Spiritual</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-2180">and</w>
     <w lemma="temporal" pos="j" xml:id="A66333-001-a-2190">Temporal</w>
     <pc xml:id="A66333-001-a-2200">,</pc>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-2210">and</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-2220">the</w>
     <w lemma="knight" pos="n2" xml:id="A66333-001-a-2230">Knights</w>
     <pc xml:id="A66333-001-a-2240">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A66333-001-a-2250">Citizens</w>
     <w lemma="and" pos="cc" xml:id="A66333-001-a-2260">and</w>
     <w lemma="burgess" pos="n2" xml:id="A66333-001-a-2270">Burgesses</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-2280">of</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-2290">the</w>
     <w lemma="house" pos="n1" xml:id="A66333-001-a-2300">House</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-2310">of</w>
     <w lemma="commons" pos="n2" xml:id="A66333-001-a-2320">Commons</w>
     <w lemma="to" pos="prt" xml:id="A66333-001-a-2330">to</w>
     <w lemma="give" pos="vvi" xml:id="A66333-001-a-2340">give</w>
     <w lemma="their" pos="po" xml:id="A66333-001-a-2350">their</w>
     <w lemma="attendance" pos="n1" xml:id="A66333-001-a-2360">Attendance</w>
     <w lemma="at" pos="acp" xml:id="A66333-001-a-2370">at</w>
     <hi xml:id="A66333-e190">
      <w lemma="Westminster" pos="nn1" xml:id="A66333-001-a-2380">Westminster</w>
     </hi>
     <w lemma="on" pos="acp" xml:id="A66333-001-a-2390">on</w>
     <w lemma="the" pos="d" xml:id="A66333-001-a-2400">the</w>
     <w lemma="say" pos="vvd" xml:id="A66333-001-a-2410">said</w>
     <w lemma="six" pos="ord" xml:id="A66333-001-a-2420">Sixth</w>
     <w lemma="day" pos="n1" xml:id="A66333-001-a-2430">day</w>
     <w lemma="of" pos="acp" xml:id="A66333-001-a-2440">of</w>
     <hi xml:id="A66333-e200">
      <w lemma="November" pos="nn1" xml:id="A66333-001-a-2450">November</w>
     </hi>
     <w lemma="according" pos="av-j" xml:id="A66333-001-a-2460">accordingly</w>
     <pc unit="sentence" xml:id="A66333-001-a-2470">.</pc>
    </p>
    <closer xml:id="A66333-e210">
     <dateline xml:id="A66333-e220">
      <w lemma="give" pos="vvn" xml:id="A66333-001-a-2480">Given</w>
      <w lemma="at" pos="acp" xml:id="A66333-001-a-2490">at</w>
      <w lemma="our" pos="po" xml:id="A66333-001-a-2500">Our</w>
      <w lemma="court" pos="n1" xml:id="A66333-001-a-2510">Court</w>
      <w lemma="at" pos="acp" xml:id="A66333-001-a-2520">at</w>
      <hi xml:id="A66333-e230">
       <w lemma="Whitehall" pos="nn1" xml:id="A66333-001-a-2530">Whitehall</w>
      </hi>
      <date xml:id="A66333-e240">
       <w lemma="the" pos="d" xml:id="A66333-001-a-2540">the</w>
       <w lemma="four" pos="ord" xml:id="A66333-001-a-2550">Fourth</w>
       <w lemma="day" pos="n1" xml:id="A66333-001-a-2560">Day</w>
       <w lemma="of" pos="acp" xml:id="A66333-001-a-2570">of</w>
       <hi xml:id="A66333-e250">
        <w lemma="October" pos="nn1" xml:id="A66333-001-a-2580">October</w>
        <pc xml:id="A66333-001-a-2590">,</pc>
       </hi>
       <w lemma="1694." pos="crd" xml:id="A66333-001-a-2600">1694.</w>
       <pc unit="sentence" xml:id="A66333-001-a-2610"/>
       <w lemma="in" pos="acp" xml:id="A66333-001-a-2620">In</w>
       <w lemma="the" pos="d" xml:id="A66333-001-a-2630">the</w>
       <w lemma="six" pos="ord" xml:id="A66333-001-a-2640">Sixth</w>
       <w lemma="year" pos="n1" xml:id="A66333-001-a-2650">Year</w>
       <w lemma="of" pos="acp" xml:id="A66333-001-a-2660">of</w>
       <w lemma="our" pos="po" xml:id="A66333-001-a-2670">Our</w>
       <w lemma="reign" pos="n1" xml:id="A66333-001-a-2680">Reign</w>
       <pc unit="sentence" xml:id="A66333-001-a-2690">.</pc>
      </date>
     </dateline>
     <lb xml:id="A66333-e260"/>
     <hi xml:id="A66333-e270">
      <w lemma="God" pos="nn1" xml:id="A66333-001-a-2700">God</w>
      <w lemma="save" pos="acp" xml:id="A66333-001-a-2710">save</w>
      <w lemma="king" pos="n1" xml:id="A66333-001-a-2720">King</w>
      <w lemma="William" pos="nn1" xml:id="A66333-001-a-2730">William</w>
      <w lemma="and" pos="cc" xml:id="A66333-001-a-2740">and</w>
      <w lemma="queen" pos="n1" xml:id="A66333-001-a-2750">Queen</w>
      <w lemma="Mary" pos="nn1" xml:id="A66333-001-a-2760">Mary</w>
      <pc unit="sentence" xml:id="A66333-001-a-2770">.</pc>
     </hi>
    </closer>
   </div>
  </body>
  <back xml:id="A66333-e280">
   <div type="colophon" xml:id="A66333-e290">
    <p xml:id="A66333-e300">
     <hi xml:id="A66333-e310">
      <hi xml:id="A66333-e320">
       <w lemma="LONDON" pos="nn1" xml:id="A66333-001-a-2780">LONDON</w>
       <pc xml:id="A66333-001-a-2790">,</pc>
      </hi>
      <w lemma="print" pos="vvn" xml:id="A66333-001-a-2800">Printed</w>
      <w lemma="by" pos="acp" xml:id="A66333-001-a-2810">by</w>
      <hi xml:id="A66333-e330">
       <w lemma="Charles" pos="nn1" xml:id="A66333-001-a-2820">Charles</w>
       <w lemma="bill" pos="n1" xml:id="A66333-001-a-2830">Bill</w>
       <pc xml:id="A66333-001-a-2840">,</pc>
      </hi>
      <w lemma="and" pos="cc" xml:id="A66333-001-a-2850">and</w>
      <w lemma="the" pos="d" xml:id="A66333-001-a-2860">the</w>
      <w lemma="executrix" pos="n1" xml:id="A66333-001-a-2870">Executrix</w>
      <w lemma="of" pos="acp" xml:id="A66333-001-a-2880">of</w>
      <hi xml:id="A66333-e340">
       <w lemma="Thomas" pos="nn1" xml:id="A66333-001-a-2890">Thomas</w>
       <w lemma="newcomb" pos="nn1" xml:id="A66333-001-a-2900">Newcomb</w>
      </hi>
      <w lemma="decease" pos="vvn" reg="deceased" xml:id="A66333-001-a-2910">deceas'd</w>
      <pc xml:id="A66333-001-a-2920">,</pc>
      <w lemma="printer" pos="n2" xml:id="A66333-001-a-2930">Printers</w>
      <w lemma="to" pos="acp" xml:id="A66333-001-a-2940">to</w>
      <w lemma="the" pos="d" xml:id="A66333-001-a-2950">the</w>
      <w lemma="king" pos="n1" xml:id="A66333-001-a-2960">King</w>
      <w lemma="and" pos="cc" xml:id="A66333-001-a-2970">and</w>
      <w lemma="queen" pos="n2" xml:id="A66333-001-a-2980">Queens</w>
      <w lemma="most" pos="avs-d" xml:id="A66333-001-a-2990">most</w>
      <w lemma="excellent" pos="j" xml:id="A66333-001-a-3000">Excellent</w>
      <w lemma="majesty" pos="n2" xml:id="A66333-001-a-3010">Majesties</w>
      <pc unit="sentence" xml:id="A66333-001-a-3020">.</pc>
      <w lemma="1694." pos="crd" xml:id="A66333-001-a-3030">1694.</w>
      <pc unit="sentence" xml:id="A66333-001-a-3040"/>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
