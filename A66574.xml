<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66574">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To His Excellence Richard Earle of Arran &amp;c. Lord Deputy of Ireland, a poem</title>
    <author>Wilson, John, 1626-1696.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66574 of text R220027 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing W2924). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 6 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66574</idno>
    <idno type="STC">Wing W2924</idno>
    <idno type="STC">ESTC R220027</idno>
    <idno type="EEBO-CITATION">99831465</idno>
    <idno type="PROQUEST">99831465</idno>
    <idno type="VID">35928</idno>
    <idno type="PROQUESTGOID">2240935964</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66574)</note>
    <note>Transcribed from: (Early English Books Online ; image set 35928)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 2121:16)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To His Excellence Richard Earle of Arran &amp;c. Lord Deputy of Ireland, a poem</title>
      <author>Wilson, John, 1626-1696.</author>
     </titleStmt>
     <extent>4 p.</extent>
     <publicationStmt>
      <publisher>printed at His Majesties printing-house for Joseph Wild bookseller in Castle-Street,</publisher>
      <pubPlace>Dublin :</pubPlace>
      <date>1682.</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed at end: J. Wilson.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Arran, Richard Butler, -- Earl of, d. 1686 -- Poetry -- Early works to 1800.</term>
     <term>Ormonde, James Butler, -- Duke of, 1610-1688 -- Poetry -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To His Excellence Richard Earle of Arran &amp;c. Lord Deputy of Ireland, a poem.</ep:title>
    <ep:author>Wilson, John, </ep:author>
    <ep:publicationYear>1682</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>6</ep:pageCount>
    <ep:wordCount>408</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2004-12</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2005-02</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2005-03</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2005-03</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2005-04</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66574-t">
  <front xml:id="A66574-e0">
   <div type="title_page" xml:id="A66574-e10">
    <pb facs="tcp:35928:1" rend="simple:additions" xml:id="A66574-001-a"/>
    <p xml:id="A66574-e20">
     <w lemma="to" pos="acp" xml:id="A66574-001-a-0010">TO</w>
     <w lemma="his" pos="po" xml:id="A66574-001-a-0020">HIS</w>
     <w lemma="excellence" pos="n1" xml:id="A66574-001-a-0030">EXCELLENCE</w>
     <w lemma="richard" pos="nn1" rend="hi" xml:id="A66574-001-a-0040">RICHARD</w>
     <w lemma="earl" pos="n1" reg="Earl" xml:id="A66574-001-a-0050">Earle</w>
     <w lemma="of" pos="acp" xml:id="A66574-001-a-0060">of</w>
     <w lemma="Arran" pos="nn1" rend="hi" xml:id="A66574-001-a-0070">ARRAN</w>
     <w lemma="etc." pos="ab" reg="etc." xml:id="A66574-001-a-0080">&amp;c.</w>
     <w lemma="lord" pos="n1" xml:id="A66574-001-a-0090">LORD</w>
     <w lemma="deputy" pos="n1" xml:id="A66574-001-a-0100">DEPUTY</w>
     <w lemma="of" pos="acp" xml:id="A66574-001-a-0110">of</w>
     <w lemma="IRELAND" pos="nn1" rend="hi" xml:id="A66574-001-a-0120">IRELAND</w>
     <pc xml:id="A66574-001-a-0130">,</pc>
    </p>
    <p xml:id="A66574-e60">
     <w lemma="a" pos="d" xml:id="A66574-001-a-0140">A</w>
     <w lemma="poem" pos="n1" xml:id="A66574-001-a-0150">POEM</w>
     <pc unit="sentence" xml:id="A66574-001-a-0160">.</pc>
    </p>
    <q xml:id="A66574-e70">
     <l xml:id="A66574-e80">
      <pc unit="sentence" xml:id="A66574-001-a-0170">—</pc>
      <w lemma="nec" pos="fla" xml:id="A66574-001-a-0180">Nec</w>
      <w lemma="deficit" pos="fla" xml:id="A66574-001-a-0190">deficit</w>
      <w lemma="alter" pos="fla" xml:id="A66574-001-a-0200">alter</w>
     </l>
     <l xml:id="A66574-e90">
      <w lemma="aureus" pos="nn1" xml:id="A66574-001-a-0210">Aureus</w>
      <pc xml:id="A66574-001-a-0220">,</pc>
      <w lemma="&amp;" pos="cc" xml:id="A66574-001-a-0230">&amp;</w>
      <w lemma="simili" pos="fla" xml:id="A66574-001-a-0240">simili</w>
      <w lemma="frondescit" pos="fla" xml:id="A66574-001-a-0250">frondescit</w>
      <w lemma="virga" pos="fla" xml:id="A66574-001-a-0260">virga</w>
      <w lemma="metallo" pos="fla" xml:id="A66574-001-a-0270">metallo</w>
      <pc unit="sentence" xml:id="A66574-001-a-0280">.</pc>
     </l>
    </q>
    <p xml:id="A66574-e100">
     <w lemma="Dublin" pos="nn1" reg="DUBLIN" xml:id="A66574-001-a-0290">DVBLIN</w>
     <pc xml:id="A66574-001-a-0300">,</pc>
     <w lemma="print" pos="vvn" xml:id="A66574-001-a-0310">Printed</w>
     <w lemma="at" pos="acp" xml:id="A66574-001-a-0320">at</w>
     <w lemma="his" pos="po" xml:id="A66574-001-a-0330">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A66574-001-a-0340">Majesties</w>
     <w lemma="printing-house" pos="n1" xml:id="A66574-001-a-0350">Printing-House</w>
     <w lemma="for" pos="acp" xml:id="A66574-001-a-0360">for</w>
     <hi xml:id="A66574-e110">
      <w lemma="Joseph" pos="nn1" xml:id="A66574-001-a-0370">Joseph</w>
      <w lemma="wild" pos="j" xml:id="A66574-001-a-0380">Wild</w>
     </hi>
     <w lemma="bookseller" pos="n1" xml:id="A66574-001-a-0390">Bookseller</w>
     <w lemma="in" pos="acp" xml:id="A66574-001-a-0400">in</w>
     <w lemma="castle-street" pos="n1" rend="hi" xml:id="A66574-001-a-0410">Castle-Street</w>
     <pc unit="sentence" xml:id="A66574-001-a-0420">.</pc>
     <w lemma="1682." pos="crd" xml:id="A66574-001-a-0430">1682.</w>
     <pc unit="sentence" xml:id="A66574-001-a-0440"/>
    </p>
   </div>
  </front>
  <body xml:id="A66574-e130">
   <div type="poem" xml:id="A66574-e140">
    <pb facs="tcp:35928:2" xml:id="A66574-002-a"/>
    <pb facs="tcp:35928:3" rend="simple:additions" xml:id="A66574-003-a"/>
    <head xml:id="A66574-e150">
     <w lemma="to" pos="prt" xml:id="A66574-003-a-0010">To</w>
     <w lemma="his" pos="po" xml:id="A66574-003-a-0020">His</w>
     <w lemma="excellence" pos="n1" xml:id="A66574-003-a-0030">Excellence</w>
     <w lemma="Richard" pos="nn1" rend="hi" xml:id="A66574-003-a-0040">Richard</w>
     <w lemma="earl" pos="n1" xml:id="A66574-003-a-0050">Earl</w>
     <w lemma="of" pos="acp" xml:id="A66574-003-a-0060">of</w>
     <w lemma="Arran" pos="nn1" rend="hi" xml:id="A66574-003-a-0070">Arran</w>
     <pc xml:id="A66574-003-a-0080">,</pc>
     <w lemma="lord" pos="n1" xml:id="A66574-003-a-0090">Lord</w>
     <w lemma="deputy" pos="n1" xml:id="A66574-003-a-0100">Deputy</w>
     <w lemma="of" pos="acp" xml:id="A66574-003-a-0110">of</w>
     <w lemma="Ireland" pos="nn1" rend="hi" xml:id="A66574-003-a-0120">Ireland</w>
     <pc xml:id="A66574-003-a-0130">:</pc>
     <w lemma="on" pos="acp" xml:id="A66574-003-a-0140">On</w>
     <w lemma="the" pos="d" xml:id="A66574-003-a-0150">the</w>
     <w lemma="occasion" pos="n1" xml:id="A66574-003-a-0160">occasion</w>
     <w lemma="of" pos="acp" xml:id="A66574-003-a-0170">of</w>
     <w lemma="his" pos="po" xml:id="A66574-003-a-0180">His</w>
     <w lemma="grace" pos="n1" xml:id="A66574-003-a-0190">Grace</w>
     <w lemma="James" pos="nn1" rend="hi" xml:id="A66574-003-a-0200">James</w>
     <w lemma="duke" pos="n1" xml:id="A66574-003-a-0210">Duke</w>
     <w lemma="of" pos="acp" xml:id="A66574-003-a-0220">of</w>
     <hi xml:id="A66574-e200">
      <w lemma="ormonde" pos="nn1" xml:id="A66574-003-a-0230">Ormonde</w>
      <w lemma="etc." pos="ab" reg="etc." xml:id="A66574-003-a-0240">&amp;c.</w>
     </hi>
     <pc rend="follows-hi" unit="sentence" xml:id="A66574-003-a-0250"/>
     <w lemma="lord" pos="n1" xml:id="A66574-003-a-0260">Lord</w>
     <w lemma="lieutenant" pos="n1" xml:id="A66574-003-a-0270">Lieutenant</w>
     <w lemma="of" pos="acp" xml:id="A66574-003-a-0280">of</w>
     <w lemma="the" pos="d" xml:id="A66574-003-a-0290">the</w>
     <w lemma="same" pos="d" xml:id="A66574-003-a-0300">same</w>
     <pc xml:id="A66574-003-a-0310">,</pc>
     <pc join="right" xml:id="A66574-003-a-0320">(</pc>
     <w lemma="his" pos="po" xml:id="A66574-003-a-0330">His</w>
     <w lemma="father" pos="n1" xml:id="A66574-003-a-0340">father</w>
     <w lemma="be" pos="vvz" rend="hi" xml:id="A66574-003-a-0350">'s</w>
     <pc xml:id="A66574-003-a-0360">)</pc>
     <w lemma="go" pos="vvg" xml:id="A66574-003-a-0370">going</w>
     <w lemma="for" pos="acp" xml:id="A66574-003-a-0380">for</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A66574-003-a-0390">England</w>
     <pc xml:id="A66574-003-a-0400">,</pc>
     <w lemma="and" pos="cc" xml:id="A66574-003-a-0410">And</w>
     <w lemma="leave" pos="vvg" xml:id="A66574-003-a-0420">leaving</w>
     <w lemma="the" pos="d" xml:id="A66574-003-a-0430">the</w>
     <w lemma="government" pos="n1" xml:id="A66574-003-a-0440">Government</w>
     <w lemma="to" pos="acp" xml:id="A66574-003-a-0450">to</w>
     <w lemma="he" pos="pno" xml:id="A66574-003-a-0460">him</w>
     <pc unit="sentence" xml:id="A66574-003-a-0470">.</pc>
    </head>
    <lg xml:id="A66574-e230">
     <l xml:id="A66574-e240">
      <w lemma="hence" pos="av" xml:id="A66574-003-a-0480">HEnce</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-0490">the</w>
      <w lemma="nice" pos="j" xml:id="A66574-003-a-0500">nice</w>
      <w lemma="wit" pos="n2" reg="Wits" xml:id="A66574-003-a-0510">Witts</w>
      <w lemma="that" pos="cs" xml:id="A66574-003-a-0520">that</w>
      <w lemma="be" pos="vvb" xml:id="A66574-003-a-0530">are</w>
      <w lemma="so" pos="av" xml:id="A66574-003-a-0540">so</w>
      <w lemma="squeamish" pos="j" xml:id="A66574-003-a-0550">squeamish</w>
      <w lemma="grow" pos="vvn" xml:id="A66574-003-a-0560">grown</w>
      <pc xml:id="A66574-003-a-0570">,</pc>
     </l>
     <l xml:id="A66574-e250">
      <w lemma="nothing" pos="pix" xml:id="A66574-003-a-0580">Nothing</w>
      <w lemma="will" pos="vmb" xml:id="A66574-003-a-0590">will</w>
      <w lemma="down" pos="acp" xml:id="A66574-003-a-0600">down</w>
      <w lemma="with" pos="acp" xml:id="A66574-003-a-0610">with</w>
      <w lemma="they" pos="pno" xml:id="A66574-003-a-0620">them</w>
      <pc xml:id="A66574-003-a-0630">,</pc>
      <w lemma="but" pos="acp" xml:id="A66574-003-a-0640">but</w>
      <w lemma="what" pos="crq" xml:id="A66574-003-a-0650">what</w>
      <w lemma="be" pos="vvz" rend="hi" xml:id="A66574-003-a-0660">'s</w>
      <w lemma="their" pos="po" xml:id="A66574-003-a-0670">their</w>
      <w lemma="own" pos="d" xml:id="A66574-003-a-0680">own</w>
      <pc xml:id="A66574-003-a-0690">:</pc>
     </l>
     <l xml:id="A66574-e270">
      <w lemma="it" pos="pn" xml:id="A66574-003-a-0700">It</w>
      <w lemma="have" pos="vvz" xml:id="A66574-003-a-0710">has</w>
      <w lemma="be" pos="vvn" xml:id="A66574-003-a-0720">been</w>
      <w lemma="say" pos="vvn" xml:id="A66574-003-a-0730">said</w>
      <pc join="right" xml:id="A66574-003-a-0740">(</pc>
      <w lemma="yet" pos="av" xml:id="A66574-003-a-0750">yet</w>
      <w lemma="tax" pos="vvn" reg="taxed" xml:id="A66574-003-a-0760">tax'd</w>
      <pc xml:id="A66574-003-a-0770">)</pc>
      <w lemma="i" pos="pns" xml:id="A66574-003-a-0780">I</w>
      <w lemma="freeze" pos="vvb" reg="frieze" rend="hi" xml:id="A66574-003-a-0790">freeze</w>
      <pc xml:id="A66574-003-a-0800">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-003-a-0810">and</w>
      <w lemma="burn" pos="vvi" rend="hi" xml:id="A66574-003-a-0820">burn</w>
      <pc xml:id="A66574-003-a-0830">,</pc>
     </l>
     <l xml:id="A66574-e300">
      <w lemma="and" pos="cc" xml:id="A66574-003-a-0840">And</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-0850">the</w>
      <w lemma="same" pos="d" xml:id="A66574-003-a-0860">same</w>
      <w lemma="instant" pos="n1-j" xml:id="A66574-003-a-0870">instant</w>
      <pc xml:id="A66574-003-a-0880">,</pc>
      <w lemma="both" pos="av-d" xml:id="A66574-003-a-0890">both</w>
      <w lemma="rejoice" pos="vvb" reg="rejoice" xml:id="A66574-003-a-0900">rejoyce</w>
      <pc xml:id="A66574-003-a-0910">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-003-a-0920">and</w>
      <w lemma="mourn" pos="vvi" xml:id="A66574-003-a-0930">mourn</w>
     </l>
     <l xml:id="A66574-e310">
      <w lemma="and" pos="cc" xml:id="A66574-003-a-0940">And</w>
      <w lemma="why" pos="crq" xml:id="A66574-003-a-0950">why</w>
      <pc join="right" xml:id="A66574-003-a-0960">(</pc>
      <w lemma="i" pos="pns" xml:id="A66574-003-a-0970">I</w>
      <w lemma="pray" pos="vvb" xml:id="A66574-003-a-0980">pray</w>
      <pc xml:id="A66574-003-a-0990">)</pc>
      <w lemma="may" pos="vmbx" xml:id="A66574-003-a-1000">mayn't</w>
      <w lemma="different" pos="j" xml:id="A66574-003-a-1010">different</w>
      <w lemma="note" pos="n2" xml:id="A66574-003-a-1020">notes</w>
      <w lemma="agree" pos="vvi" xml:id="A66574-003-a-1030">agree</w>
      <pc unit="sentence" xml:id="A66574-003-a-1040">?</pc>
     </l>
     <l xml:id="A66574-e320">
      <w lemma="take" pos="vvb" xml:id="A66574-003-a-1050">Take</w>
      <w lemma="away" pos="av" xml:id="A66574-003-a-1060">away</w>
      <w lemma="discord" pos="n2" xml:id="A66574-003-a-1070">discords</w>
      <pc xml:id="A66574-003-a-1080">,</pc>
      <w join="right" lemma="where" pos="crq" xml:id="A66574-003-a-1090">where</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A66574-003-a-1091">'s</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-1100">the</w>
      <w lemma="harmony" pos="n1" xml:id="A66574-003-a-1110">Harmony</w>
      <pc unit="sentence" xml:id="A66574-003-a-1120">?</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e330">
     <l xml:id="A66574-e340">
      <w lemma="both" pos="av-d" xml:id="A66574-003-a-1130">Both</w>
      <w lemma="be" pos="vvb" xml:id="A66574-003-a-1140">are</w>
      <w lemma="meet" pos="vvn" xml:id="A66574-003-a-1150">met</w>
      <w lemma="here" pos="av" xml:id="A66574-003-a-1160">here</w>
      <pc xml:id="A66574-003-a-1170">:</pc>
      <w lemma="we" pos="pns" xml:id="A66574-003-a-1180">We</w>
      <w lemma="mourn" pos="vvb" xml:id="A66574-003-a-1190">mourn</w>
      <pc xml:id="A66574-003-a-1200">,</pc>
      <w lemma="one" pos="crd" xml:id="A66574-003-a-1210">one</w>
      <w lemma="sun" pos="n1" rend="hi" xml:id="A66574-003-a-1220">Sun</w>
      <w lemma="go" pos="vvn" xml:id="A66574-003-a-1230">gone</w>
      <w lemma="east" pos="n1" rend="hi" xml:id="A66574-003-a-1240">East</w>
      <pc xml:id="A66574-003-a-1250">,</pc>
     </l>
     <l xml:id="A66574-e370">
      <w lemma="and" pos="cc" xml:id="A66574-003-a-1260">And</w>
      <w lemma="joy" pos="n1" xml:id="A66574-003-a-1270">joy</w>
      <pc xml:id="A66574-003-a-1280">,</pc>
      <w lemma="another" pos="d" xml:id="A66574-003-a-1290">Another</w>
      <w lemma="rise" pos="n1-vg" xml:id="A66574-003-a-1300">rising</w>
      <w lemma="in" pos="acp" xml:id="A66574-003-a-1310">in</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-1320">the</w>
      <w lemma="west" pos="n1" rend="hi" xml:id="A66574-003-a-1330">West</w>
      <pc xml:id="A66574-003-a-1340">:</pc>
     </l>
     <l xml:id="A66574-e390">
      <w lemma="such" pos="d" xml:id="A66574-003-a-1350">Such</w>
      <pc xml:id="A66574-003-a-1360">—</pc>
      <w lemma="such" pos="d" xml:id="A66574-003-a-1370">such</w>
      <pc xml:id="A66574-003-a-1380">,</pc>
      <w lemma="as" pos="acp" xml:id="A66574-003-a-1390">as</w>
      <w lemma="have" pos="vvd" xml:id="A66574-003-a-1400">had</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-1410">the</w>
      <w lemma="ancient" pos="j" reg="Ancient" xml:id="A66574-003-a-1420">Antient</w>
      <w lemma="persian" pos="jnn" rend="hi" xml:id="A66574-003-a-1430">Persian</w>
     </l>
     <l xml:id="A66574-e410">
      <w lemma="view" pos="vvd" reg="Viewed" xml:id="A66574-003-a-1440">View'd</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-1450">the</w>
      <w lemma="Parelia" pos="nn1" rend="hi" xml:id="A66574-003-a-1460">Parelia</w>
      <pc xml:id="A66574-003-a-1470">,</pc>
      <w lemma="this" pos="d" xml:id="A66574-003-a-1480">this</w>
      <w lemma="double" pos="j" xml:id="A66574-003-a-1490">double</w>
      <w lemma="sun" pos="n1" rend="hi" xml:id="A66574-003-a-1500">Sun</w>
     </l>
     <l xml:id="A66574-e440">
      <w lemma="have" pos="vvd" xml:id="A66574-003-a-1510">Had</w>
      <w lemma="make" pos="vvn" xml:id="A66574-003-a-1520">made</w>
      <w lemma="he" pos="pno" xml:id="A66574-003-a-1530">him</w>
      <w lemma="stagger" pos="vvi" xml:id="A66574-003-a-1540">stagger</w>
      <w lemma="at" pos="acp" xml:id="A66574-003-a-1550">at</w>
      <w lemma="the" pos="d" xml:id="A66574-003-a-1560">the</w>
      <w lemma="smart" pos="j" xml:id="A66574-003-a-1570">smart</w>
      <w lemma="surprise" pos="n1" xml:id="A66574-003-a-1580">surprise</w>
      <pc xml:id="A66574-003-a-1590">,</pc>
     </l>
     <l xml:id="A66574-e450">
      <w lemma="nor" pos="ccx" xml:id="A66574-003-a-1600">Nor</w>
      <w lemma="yet" pos="av" xml:id="A66574-003-a-1610">yet</w>
      <w lemma="resolve" pos="vvn" reg="resolved" xml:id="A66574-003-a-1620">resolv'd</w>
      <pc xml:id="A66574-003-a-1630">,</pc>
      <w lemma="divide" pos="vvb" xml:id="A66574-003-a-1640">divide</w>
      <w lemma="his" pos="po" xml:id="A66574-003-a-1650">his</w>
      <w lemma="sacrifice" pos="n1" xml:id="A66574-003-a-1660">sacrifice</w>
      <pc unit="sentence" xml:id="A66574-003-a-1670">.</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e460">
     <pb facs="tcp:35928:4" n="4" xml:id="A66574-004-a"/>
     <l xml:id="A66574-e470">
      <w join="right" lemma="it" pos="pn" xml:id="A66574-004-a-0010">'T</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A66574-004-a-0011">is</w>
      <w lemma="now" pos="av" xml:id="A66574-004-a-0020">now</w>
      <w lemma="past" pos="acp" xml:id="A66574-004-a-0030">past</w>
      <w lemma="twenty" pos="crd" xml:id="A66574-004-a-0040">twenty</w>
      <w lemma="time" pos="n2" xml:id="A66574-004-a-0050">times</w>
      <w lemma="since" pos="acp" xml:id="A66574-004-a-0060">since</w>
      <w lemma="the" pos="d" xml:id="A66574-004-a-0070">th'</w>
      <w lemma="ormonde" pos="nn1" rend="hi" xml:id="A66574-004-a-0080">Ormonde</w>
      <w lemma="stem" pos="n1" xml:id="A66574-004-a-0090">stem</w>
     </l>
     <l xml:id="A66574-e490">
      <w lemma="first" pos="ord" xml:id="A66574-004-a-0100">First</w>
      <w lemma="branch" pos="vvn" reg="branched" xml:id="A66574-004-a-0110">brancht</w>
      <w lemma="itself" pos="pr" reg="itself" xml:id="A66574-004-a-0120">it self</w>
      <w lemma="in" pos="acp" xml:id="A66574-004-a-0140">in</w>
      <w lemma="such" pos="d" xml:id="A66574-004-a-0150">such</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-0160">a</w>
      <w lemma="princely" pos="j" xml:id="A66574-004-a-0170">Princely</w>
      <w lemma="beam" pos="n1" xml:id="A66574-004-a-0180">Beam</w>
      <pc xml:id="A66574-004-a-0190">;</pc>
     </l>
     <l xml:id="A66574-e500">
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0200">And</w>
      <w lemma="may" pos="vmb" xml:id="A66574-004-a-0210">may</w>
      <w lemma="it" pos="pn" xml:id="A66574-004-a-0220">it</w>
      <w lemma="yet" pos="av" xml:id="A66574-004-a-0230">yet</w>
      <w lemma="increase" pos="vvi" reg="increase" xml:id="A66574-004-a-0240">encrease</w>
      <pc xml:id="A66574-004-a-0250">;</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0260">and</w>
      <w lemma="multiply" pos="vvb" xml:id="A66574-004-a-0270">multiply</w>
     </l>
     <l xml:id="A66574-e510">
      <w lemma="it" pos="po" reg="Its" xml:id="A66574-004-a-0280">It's</w>
      <w lemma="scatter" pos="j-vn" reg="scattered" xml:id="A66574-004-a-0290">scatter'd</w>
      <w lemma="ray" pos="n2" reg="rays" xml:id="A66574-004-a-0300">rayes</w>
      <pc xml:id="A66574-004-a-0310">,</pc>
      <w lemma="into" pos="acp" xml:id="A66574-004-a-0320">into</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-0330">a</w>
      <w lemma="galaxy" pos="n1" rend="hi" xml:id="A66574-004-a-0340">Galaxy</w>
      <pc unit="sentence" xml:id="A66574-004-a-0350">.</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e530">
     <l xml:id="A66574-e540">
      <w lemma="spread-eagle" pos="n2" rend="hi" xml:id="A66574-004-a-0360">Spread-Eagles</w>
      <w lemma="join" pos="vvb" reg="join" xml:id="A66574-004-a-0370">joyn</w>
      <w lemma="in" pos="acp" xml:id="A66574-004-a-0380">in</w>
      <w lemma="body" pos="n1" xml:id="A66574-004-a-0390">body</w>
      <pc xml:id="A66574-004-a-0400">;</pc>
      <w lemma="Lucifer" pos="nn1" rend="hi" xml:id="A66574-004-a-0410">Lucifer</w>
      <pc xml:id="A66574-004-a-0420">,</pc>
     </l>
     <l xml:id="A66574-e570">
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0430">And</w>
      <w lemma="Vesper" pos="nn1" rend="hi" xml:id="A66574-004-a-0440">Vesper</w>
      <pc xml:id="A66574-004-a-0450">,</pc>
      <w lemma="be" pos="vvb" xml:id="A66574-004-a-0460">are</w>
      <w lemma="the" pos="d" xml:id="A66574-004-a-0470">the</w>
      <w lemma="same" pos="d" xml:id="A66574-004-a-0480">same</w>
      <w lemma="alternate" pos="j" xml:id="A66574-004-a-0490">alternate</w>
      <w lemma="star" pos="n1" xml:id="A66574-004-a-0500">Star</w>
      <pc xml:id="A66574-004-a-0510">:</pc>
     </l>
     <l xml:id="A66574-e590">
      <w lemma="the" pos="d" xml:id="A66574-004-a-0520">The</w>
      <w lemma="element" pos="n2" xml:id="A66574-004-a-0530">Elements</w>
      <pc xml:id="A66574-004-a-0540">,</pc>
      <w lemma="Castor" pos="nn1" rend="hi" xml:id="A66574-004-a-0550">Castor</w>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0560">and</w>
      <w lemma="Pollux" pos="nn1" rend="hi" xml:id="A66574-004-a-0570">Pollux</w>
      <w lemma="too" pos="av" xml:id="A66574-004-a-0580">too</w>
      <pc xml:id="A66574-004-a-0590">,</pc>
     </l>
     <l xml:id="A66574-e620">
      <w lemma="relieve" pos="vvb" xml:id="A66574-004-a-0600">Relieve</w>
      <w lemma="each" pos="d" xml:id="A66574-004-a-0610">each</w>
      <w lemma="other" pos="pi-d" xml:id="A66574-004-a-0620">other</w>
      <pc xml:id="A66574-004-a-0630">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0640">and</w>
      <w lemma="in" pos="acp" xml:id="A66574-004-a-0650">in</w>
      <w lemma="that" pos="d" xml:id="A66574-004-a-0660">that</w>
      <pc xml:id="A66574-004-a-0670">,</pc>
      <w lemma="still" pos="av" xml:id="A66574-004-a-0680">still</w>
      <w lemma="new" pos="j" xml:id="A66574-004-a-0690">new</w>
      <pc unit="sentence" xml:id="A66574-004-a-0700">.</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e630">
     <l xml:id="A66574-e640">
      <w lemma="nature" pos="n1" xml:id="A66574-004-a-0710">Nature</w>
      <w lemma="have" pos="vvd" xml:id="A66574-004-a-0720">had</w>
      <w lemma="never" pos="avx" xml:id="A66574-004-a-0730">never</w>
      <w lemma="make" pos="vvn" xml:id="A66574-004-a-0740">made</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-0750">a</w>
      <w lemma="second" pos="ord" xml:id="A66574-004-a-0760">second</w>
      <w lemma="day" pos="n1" xml:id="A66574-004-a-0770">day</w>
      <pc xml:id="A66574-004-a-0780">,</pc>
     </l>
     <l xml:id="A66574-e650">
      <w lemma="without" pos="acp" xml:id="A66574-004-a-0790">Without</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-0800">a</w>
      <w lemma="night" pos="ng1" reg="night's" xml:id="A66574-004-a-0810">nights</w>
      <w lemma="repose" pos="n1" xml:id="A66574-004-a-0820">repose</w>
      <pc xml:id="A66574-004-a-0830">;</pc>
      <w lemma="that" pos="cs" xml:id="A66574-004-a-0840">that</w>
      <w lemma="short" pos="j" xml:id="A66574-004-a-0850">short</w>
      <w lemma="allay" pos="n1" xml:id="A66574-004-a-0860">allay</w>
      <pc xml:id="A66574-004-a-0870">,</pc>
     </l>
     <l xml:id="A66574-e660">
      <w lemma="stamp" pos="vvn" reg="Stamped" xml:id="A66574-004-a-0880">Stampt</w>
      <w lemma="we" pos="pno" xml:id="A66574-004-a-0890">us</w>
      <w lemma="another" pos="d" xml:id="A66574-004-a-0900">another</w>
      <pc xml:id="A66574-004-a-0910">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0920">and</w>
      <w lemma="that" pos="cs" xml:id="A66574-004-a-0930">that</w>
      <w lemma="timely" pos="j" xml:id="A66574-004-a-0940">timely</w>
      <w lemma="care" pos="n1" xml:id="A66574-004-a-0950">care</w>
     </l>
     <l xml:id="A66574-e670">
      <w lemma="step" pos="vvd" reg="Stepped" xml:id="A66574-004-a-0960">Stept</w>
      <w lemma="in" pos="acp" xml:id="A66574-004-a-0970">in</w>
      <pc xml:id="A66574-004-a-0980">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-0990">and</w>
      <w lemma="save" pos="vvd" reg="saved" xml:id="A66574-004-a-1000">sav'd</w>
      <w lemma="the" pos="d" xml:id="A66574-004-a-1010">the</w>
      <w lemma="infant-world" pos="ng1" xml:id="A66574-004-a-1020">Infant-World's</w>
      <w lemma="despair" pos="n1" xml:id="A66574-004-a-1030">despair</w>
      <pc unit="sentence" xml:id="A66574-004-a-1040">.</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e680">
     <l xml:id="A66574-e690">
      <w lemma="and" pos="cc" xml:id="A66574-004-a-1050">And</w>
      <w lemma="now" pos="av" xml:id="A66574-004-a-1060">now</w>
      <pc xml:id="A66574-004-a-1070">,</pc>
      <w join="right" lemma="it" pos="pn" xml:id="A66574-004-a-1080">'t</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A66574-004-a-1081">is</w>
      <w lemma="but" pos="acp" xml:id="A66574-004-a-1090">but</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-1100">a</w>
      <w lemma="day" pos="n1" xml:id="A66574-004-a-1110">day</w>
      <w lemma="from" pos="acp" xml:id="A66574-004-a-1120">from</w>
      <w lemma="sun" pos="n1" xml:id="A66574-004-a-1130">Sun</w>
      <pc xml:id="A66574-004-a-1140">,</pc>
      <w lemma="to" pos="acp" xml:id="A66574-004-a-1150">to</w>
      <w lemma="sun" pos="n1" xml:id="A66574-004-a-1160">Sun</w>
      <pc xml:id="A66574-004-a-1170">;</pc>
     </l>
     <l xml:id="A66574-e700">
      <w lemma="the" pos="d" xml:id="A66574-004-a-1180">The</w>
      <w lemma="one" pos="pi" xml:id="A66574-004-a-1190">one</w>
      <w lemma="take" pos="vvz" xml:id="A66574-004-a-1200">takes</w>
      <w lemma="up" pos="acp" xml:id="A66574-004-a-1210">up</w>
      <pc xml:id="A66574-004-a-1220">,</pc>
      <w lemma="the" pos="d" xml:id="A66574-004-a-1230">the</w>
      <w lemma="other" pos="pi-d" xml:id="A66574-004-a-1240">other</w>
      <pc xml:id="A66574-004-a-1250">,</pc>
      <w lemma="hold" pos="vvz" xml:id="A66574-004-a-1260">holds</w>
      <w lemma="it" pos="pn" xml:id="A66574-004-a-1270">it</w>
      <w lemma="on" pos="acp" xml:id="A66574-004-a-1280">on</w>
      <pc xml:id="A66574-004-a-1290">:</pc>
     </l>
     <l xml:id="A66574-e710">
      <w lemma="season" pos="n2" xml:id="A66574-004-a-1300">Seasons</w>
      <pc xml:id="A66574-004-a-1310">,</pc>
      <w lemma="to" pos="prt" xml:id="A66574-004-a-1320">to</w>
      <w lemma="season" pos="n2" xml:id="A66574-004-a-1330">Seasons</w>
      <w lemma="give" pos="vvb" xml:id="A66574-004-a-1340">give</w>
      <w lemma="a" pos="d" xml:id="A66574-004-a-1350">a</w>
      <w lemma="fresh" pos="j" xml:id="A66574-004-a-1360">fresh</w>
      <w lemma="supply" pos="n1" xml:id="A66574-004-a-1370">supply</w>
      <pc xml:id="A66574-004-a-1380">;</pc>
     </l>
     <l xml:id="A66574-e720">
      <w lemma="the" pos="d" xml:id="A66574-004-a-1390">The</w>
      <w lemma="year" pos="n1" xml:id="A66574-004-a-1400">year</w>
      <w lemma="absolve" pos="vvn" reg="absolved" xml:id="A66574-004-a-1410">absolv'd</w>
      <pc xml:id="A66574-004-a-1420">,</pc>
      <w lemma="come" pos="vvz" xml:id="A66574-004-a-1430">comes</w>
      <w lemma="the" pos="d" xml:id="A66574-004-a-1440">the</w>
      <w lemma="epiphany" pos="nn1" rend="hi" xml:id="A66574-004-a-1450">Epiphany</w>
      <pc unit="sentence" xml:id="A66574-004-a-1460">.</pc>
     </l>
    </lg>
    <lg xml:id="A66574-e740">
     <l xml:id="A66574-e750">
      <w lemma="such" pos="d" xml:id="A66574-004-a-1470">Such</w>
      <w lemma="your" pos="po" xml:id="A66574-004-a-1480">Your</w>
      <w lemma="most" pos="avs-d" xml:id="A66574-004-a-1490">most</w>
      <w lemma="noble" pos="j" xml:id="A66574-004-a-1500">noble</w>
      <w lemma="father" pos="n1" xml:id="A66574-004-a-1510">Father</w>
      <pc join="right" xml:id="A66574-004-a-1520">(</pc>
      <w lemma="sir" pos="n2" reg="Sirs" rend="hi" xml:id="A66574-004-a-1530">Sir's</w>
      <pc xml:id="A66574-004-a-1540">)</pc>
      <w lemma="with" pos="acp" xml:id="A66574-004-a-1550">with</w>
      <w lemma="you" pos="pn" xml:id="A66574-004-a-1560">you</w>
      <pc xml:id="A66574-004-a-1570">;</pc>
     </l>
     <l xml:id="A66574-e770">
      <w lemma="he" pos="pns" xml:id="A66574-004-a-1580">He</w>
      <w lemma="close" pos="vvz" xml:id="A66574-004-a-1590">closes</w>
      <w lemma="one" pos="pi" xml:id="A66574-004-a-1600">one</w>
      <pc xml:id="A66574-004-a-1610">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-1620">and</w>
      <w lemma="you" pos="pn" xml:id="A66574-004-a-1630">you</w>
      <pc xml:id="A66574-004-a-1640">,</pc>
      <w lemma="begin" pos="vvb" xml:id="A66574-004-a-1650">begin</w>
      <w lemma="the" pos="d" xml:id="A66574-004-a-1660">the</w>
      <w lemma="new" pos="j" xml:id="A66574-004-a-1670">new</w>
      <pc xml:id="A66574-004-a-1680">:</pc>
     </l>
     <l xml:id="A66574-e780">
      <w lemma="and" pos="cc" xml:id="A66574-004-a-1690">And</w>
      <pc xml:id="A66574-004-a-1700">,</pc>
      <w lemma="be" pos="vvb" xml:id="A66574-004-a-1710">be</w>
      <w lemma="his" pos="po" xml:id="A66574-004-a-1720">his</w>
      <w lemma="motion" pos="n2" xml:id="A66574-004-a-1730">Motions</w>
      <pc xml:id="A66574-004-a-1740">,</pc>
      <w lemma="you" pos="png" xml:id="A66574-004-a-1750">yours</w>
      <pc xml:id="A66574-004-a-1760">,</pc>
      <w join="right" lemma="i" pos="pns" xml:id="A66574-004-a-1770">I</w>
      <w join="left" lemma="will" pos="vmb" xml:id="A66574-004-a-1771">'ll</w>
      <w lemma="bold" pos="av-j" xml:id="A66574-004-a-1780">boldly</w>
      <w lemma="say" pos="vvi" xml:id="A66574-004-a-1790">say</w>
      <pc xml:id="A66574-004-a-1800">,</pc>
     </l>
     <l xml:id="A66574-e790">
      <w lemma="the" pos="d" xml:id="A66574-004-a-1810">The</w>
      <w lemma="sun" pos="n1" rend="hi" xml:id="A66574-004-a-1820">Sun</w>
      <w lemma="withdraw" pos="vvd" xml:id="A66574-004-a-1830">withdrew</w>
      <pc xml:id="A66574-004-a-1840">,</pc>
      <w lemma="and" pos="cc" xml:id="A66574-004-a-1850">and</w>
      <w lemma="yet" pos="av" xml:id="A66574-004-a-1860">yet</w>
      <w lemma="we" pos="pns" xml:id="A66574-004-a-1870">We</w>
      <w lemma="lose" pos="vvd" xml:id="A66574-004-a-1880">lost</w>
      <w lemma="no" pos="dx" xml:id="A66574-004-a-1890">no</w>
      <w lemma="day" pos="n1" xml:id="A66574-004-a-1900">day</w>
      <pc unit="sentence" xml:id="A66574-004-a-1910">.</pc>
     </l>
    </lg>
    <closer xml:id="A66574-e810">
     <signed xml:id="A66574-e820">
      <w lemma="j." pos="ab" xml:id="A66574-004-a-1920">J.</w>
      <w lemma="Wilson" pos="nn1" xml:id="A66574-004-a-1930">Wilson</w>
      <pc unit="sentence" xml:id="A66574-004-a-1940">.</pc>
     </signed>
    </closer>
    <pb facs="tcp:35928:5" rend="simple:additions" xml:id="A66574-005-a"/>
    <pb facs="tcp:35928:6" xml:id="A66574-006-a"/>
   </div>
  </body>
 </text>
</TEI>
