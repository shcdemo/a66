<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A66252">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>By the King and Queen, a proclamation whereas we have received information that Edmond Ludlow, commonly called Colonel Ludlow, who stands attainted of high treason ...</title>
    <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A66252 of text R5508 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing W2529). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A66252</idno>
    <idno type="STC">Wing W2529</idno>
    <idno type="STC">ESTC R5508</idno>
    <idno type="EEBO-CITATION">16283252</idno>
    <idno type="OCLC">ocm 16283252</idno>
    <idno type="VID">62669</idno>
    <idno type="PROQUESTGOID">2264215330</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A66252)</note>
    <note>Transcribed from: (Early English Books Online ; image set 62669)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 951:46 or 1602:40)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>By the King and Queen, a proclamation whereas we have received information that Edmond Ludlow, commonly called Colonel Ludlow, who stands attainted of high treason ...</title>
      <author>England and Wales. Sovereign (1689-1694 : William and Mary)</author>
      <author>Mary II, Queen of England, 1662-1694.</author>
      <author>William III, King of England, 1650-1702.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Charles Bill and Thomas Newcomb ...,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1689.</date>
     </publicationStmt>
     <notesStmt>
      <note>"Given at our court at Whitehall, the fourteenth day of November, 1689, in the first year of our reign."</note>
      <note>This item can be found at reels 951:46 and 1602:40.</note>
      <note>Reproduction of originals in the Harvard University Library and Huntington Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Ludlow, Edmund, 1617?-1692.</term>
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>By the King and Queen, a proclamation. William R. Whereas we have received information, that Edmond Ludlow, commonly called Colonel Ludlow, who stands</ep:title>
    <ep:author>England and Wales. Sovereign  </ep:author>
    <ep:publicationYear>1689</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>300</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2000-00</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2001-06</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2001-06</date><label>TCP Staff (Michigan)</label>
        Sampled and proofread
      </change>
   <change><date>2001-00</date><label>TCP Staff (Michigan)</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2001-11</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A66252-t">
  <body xml:id="A66252-e0">
   <div type="proclamation" xml:id="A66252-e10">
    <pb facs="tcp:62669:1" rend="simple:additions" xml:id="A66252-001-a"/>
    <head xml:id="A66252-e20">
     <w lemma="by" pos="acp" xml:id="A66252-001-a-0010">By</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-0020">the</w>
     <w lemma="king" pos="n1" xml:id="A66252-001-a-0030">King</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0040">and</w>
     <w lemma="queen" pos="n1" xml:id="A66252-001-a-0050">Queen</w>
     <pc xml:id="A66252-001-a-0060">,</pc>
     <w lemma="a" pos="d" xml:id="A66252-001-a-0070">A</w>
     <w lemma="proclamation" pos="n1" xml:id="A66252-001-a-0080">PROCLAMATION</w>
     <pc unit="sentence" xml:id="A66252-001-a-0090">.</pc>
    </head>
    <head xml:id="A66252-e30">
     <w lemma="William" pos="nn1" xml:id="A66252-001-a-0100">William</w>
     <w lemma="r." pos="ab" xml:id="A66252-001-a-0110">R.</w>
     <pc unit="sentence" xml:id="A66252-001-a-0120"/>
    </head>
    <p xml:id="A66252-e40">
     <w lemma="whereas" pos="cs" xml:id="A66252-001-a-0130">WHereas</w>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-0140">We</w>
     <w lemma="have" pos="vvb" xml:id="A66252-001-a-0150">have</w>
     <w lemma="receive" pos="vvn" xml:id="A66252-001-a-0160">received</w>
     <w lemma="information" pos="n1" xml:id="A66252-001-a-0170">Information</w>
     <pc xml:id="A66252-001-a-0180">,</pc>
     <w lemma="that" pos="cs" xml:id="A66252-001-a-0190">That</w>
     <hi xml:id="A66252-e50">
      <w lemma="Edmond" pos="nn1" xml:id="A66252-001-a-0200">Edmond</w>
      <w lemma="Ludlow" pos="nn1" xml:id="A66252-001-a-0210">Ludlow</w>
      <pc xml:id="A66252-001-a-0220">,</pc>
     </hi>
     <w lemma="common" pos="av-j" xml:id="A66252-001-a-0230">commonly</w>
     <w lemma="call" pos="vvn" xml:id="A66252-001-a-0240">called</w>
     <w lemma="colonel" pos="n1" xml:id="A66252-001-a-0250">Colonel</w>
     <hi xml:id="A66252-e60">
      <w lemma="Ludlow" pos="nn1" xml:id="A66252-001-a-0260">Ludlow</w>
      <pc xml:id="A66252-001-a-0270">,</pc>
     </hi>
     <w lemma="who" pos="crq" xml:id="A66252-001-a-0280">who</w>
     <w lemma="stand" pos="vvz" xml:id="A66252-001-a-0290">stands</w>
     <w lemma="attaint" pos="vvn" xml:id="A66252-001-a-0300">Attainted</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-0310">of</w>
     <w lemma="high" pos="j" xml:id="A66252-001-a-0320">High</w>
     <w lemma="treason" pos="n1" xml:id="A66252-001-a-0330">Treason</w>
     <w lemma="by" pos="acp" xml:id="A66252-001-a-0340">by</w>
     <w lemma="act" pos="n1" xml:id="A66252-001-a-0350">Act</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-0360">of</w>
     <w lemma="parliament" pos="n1" xml:id="A66252-001-a-0370">Parliament</w>
     <pc xml:id="A66252-001-a-0380">,</pc>
     <w lemma="for" pos="acp" xml:id="A66252-001-a-0390">for</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-0400">the</w>
     <w lemma="horrid" pos="j" xml:id="A66252-001-a-0410">Horrid</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0420">and</w>
     <w lemma="execrable" pos="j" xml:id="A66252-001-a-0430">Execrable</w>
     <w lemma="murder" pos="n1" xml:id="A66252-001-a-0440">Murder</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-0450">of</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-0460">Our</w>
     <w lemma="royal" pos="j" xml:id="A66252-001-a-0470">Royal</w>
     <w lemma="grandfather" pos="n1" xml:id="A66252-001-a-0480">Grandfather</w>
     <pc xml:id="A66252-001-a-0490">,</pc>
     <w lemma="have" pos="vvz" xml:id="A66252-001-a-0500">hath</w>
     <w lemma="presume" pos="vvn" xml:id="A66252-001-a-0510">presumed</w>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-0520">to</w>
     <w lemma="come" pos="vvi" xml:id="A66252-001-a-0530">come</w>
     <w lemma="into" pos="acp" xml:id="A66252-001-a-0540">into</w>
     <w lemma="this" pos="d" xml:id="A66252-001-a-0550">this</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-0560">Our</w>
     <w lemma="kingdom" pos="n1" xml:id="A66252-001-a-0570">Kingdom</w>
     <pc xml:id="A66252-001-a-0580">,</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0590">and</w>
     <w lemma="therein" pos="av" xml:id="A66252-001-a-0600">therein</w>
     <w lemma="privy" pos="av-j" xml:id="A66252-001-a-0610">privily</w>
     <w lemma="lurk" pos="vvz" xml:id="A66252-001-a-0620">Lurketh</w>
     <pc xml:id="A66252-001-a-0630">,</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0640">and</w>
     <w lemma="be" pos="vvz" xml:id="A66252-001-a-0650">is</w>
     <w lemma="conceal" pos="vvn" xml:id="A66252-001-a-0660">Concealed</w>
     <pc xml:id="A66252-001-a-0670">;</pc>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-0680">We</w>
     <w lemma="have" pos="vvb" xml:id="A66252-001-a-0690">have</w>
     <w lemma="therefore" pos="av" xml:id="A66252-001-a-0700">therefore</w>
     <w lemma="think" pos="vvd" xml:id="A66252-001-a-0710">thought</w>
     <w lemma="fit" pos="j" xml:id="A66252-001-a-0720">fit</w>
     <w lemma="by" pos="acp" xml:id="A66252-001-a-0730">by</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-0740">the</w>
     <w lemma="advice" pos="n1" xml:id="A66252-001-a-0750">Advice</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-0760">of</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-0770">Our</w>
     <w lemma="privy" pos="j" xml:id="A66252-001-a-0780">Privy</w>
     <w lemma="council" pos="n1" xml:id="A66252-001-a-0790">Council</w>
     <pc xml:id="A66252-001-a-0800">,</pc>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-0810">to</w>
     <w lemma="issue" pos="vvi" xml:id="A66252-001-a-0820">Issue</w>
     <w lemma="this" pos="d" xml:id="A66252-001-a-0830">this</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-0840">Our</w>
     <w lemma="proclamation" pos="n1" xml:id="A66252-001-a-0850">Proclamation</w>
     <pc xml:id="A66252-001-a-0860">;</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0870">And</w>
     <w lemma="do" pos="vvb" xml:id="A66252-001-a-0880">do</w>
     <w lemma="hereby" pos="av" xml:id="A66252-001-a-0890">hereby</w>
     <w lemma="command" pos="vvi" xml:id="A66252-001-a-0900">Command</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-0910">and</w>
     <w lemma="require" pos="vvi" xml:id="A66252-001-a-0920">Require</w>
     <w lemma="all" pos="d" xml:id="A66252-001-a-0930">all</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-0940">Our</w>
     <w lemma="love" pos="j-vg" xml:id="A66252-001-a-0950">Loving</w>
     <w lemma="subject" pos="n2" xml:id="A66252-001-a-0960">Subjects</w>
     <pc xml:id="A66252-001-a-0970">,</pc>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-0980">to</w>
     <w lemma="discover" pos="vvi" xml:id="A66252-001-a-0990">Discover</w>
     <pc xml:id="A66252-001-a-1000">,</pc>
     <w lemma="take" pos="vvb" xml:id="A66252-001-a-1010">Take</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-1020">and</w>
     <w lemma="apprehend" pos="vvb" xml:id="A66252-001-a-1030">Apprehend</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1040">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66252-001-a-1050">said</w>
     <hi xml:id="A66252-e70">
      <w lemma="Edmond" pos="nn1" xml:id="A66252-001-a-1060">Edmond</w>
      <w lemma="Ludlow" pos="nn1" xml:id="A66252-001-a-1070">Ludlow</w>
     </hi>
     <w lemma="where" pos="crq" xml:id="A66252-001-a-1080">where</w>
     <w lemma="ever" pos="av" xml:id="A66252-001-a-1090">ever</w>
     <w lemma="he" pos="pns" xml:id="A66252-001-a-1100">he</w>
     <w lemma="may" pos="vmb" xml:id="A66252-001-a-1110">may</w>
     <w lemma="be" pos="vvi" xml:id="A66252-001-a-1120">be</w>
     <w lemma="find" pos="vvn" xml:id="A66252-001-a-1130">found</w>
     <pc xml:id="A66252-001-a-1140">,</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-1150">and</w>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-1160">to</w>
     <w lemma="carry" pos="vvi" xml:id="A66252-001-a-1170">carry</w>
     <w lemma="he" pos="pno" xml:id="A66252-001-a-1180">him</w>
     <w lemma="before" pos="acp" xml:id="A66252-001-a-1190">before</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1200">the</w>
     <w lemma="next" pos="ord" xml:id="A66252-001-a-1210">next</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A66252-001-a-1220">Iustice</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-1230">of</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1240">the</w>
     <w lemma="peace" pos="n1" xml:id="A66252-001-a-1250">Peace</w>
     <pc xml:id="A66252-001-a-1260">,</pc>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-1270">or</w>
     <w lemma="chief" pos="j" xml:id="A66252-001-a-1280">Chief</w>
     <w lemma="magistrate" pos="n1" xml:id="A66252-001-a-1290">Magistrate</w>
     <pc xml:id="A66252-001-a-1300">,</pc>
     <w lemma="who" pos="crq" xml:id="A66252-001-a-1310">whom</w>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-1320">We</w>
     <w lemma="do" pos="vvb" xml:id="A66252-001-a-1330">do</w>
     <w lemma="hereby" pos="av" xml:id="A66252-001-a-1340">hereby</w>
     <w lemma="require" pos="vvi" xml:id="A66252-001-a-1350">Require</w>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-1360">to</w>
     <w lemma="commit" pos="vvi" xml:id="A66252-001-a-1370">Commit</w>
     <w lemma="he" pos="pno" xml:id="A66252-001-a-1380">him</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-1390">to</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1400">the</w>
     <w lemma="next" pos="ord" xml:id="A66252-001-a-1410">next</w>
     <w lemma="goal" pos="n1" xml:id="A66252-001-a-1420">Goal</w>
     <pc unit="sentence" xml:id="A66252-001-a-1430">.</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-1440">And</w>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-1450">We</w>
     <w lemma="do" pos="vvb" xml:id="A66252-001-a-1460">do</w>
     <w lemma="hereby" pos="av" xml:id="A66252-001-a-1470">hereby</w>
     <w lemma="require" pos="vvi" xml:id="A66252-001-a-1480">Require</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1490">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66252-001-a-1500">said</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="A66252-001-a-1510">Iustice</w>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-1520">or</w>
     <w lemma="other" pos="d" xml:id="A66252-001-a-1530">other</w>
     <w lemma="magistrate" pos="n1" xml:id="A66252-001-a-1540">Magistrate</w>
     <pc xml:id="A66252-001-a-1550">,</pc>
     <w lemma="immediate" pos="av-j" xml:id="A66252-001-a-1560">immediately</w>
     <w lemma="to" pos="prt" xml:id="A66252-001-a-1570">to</w>
     <w lemma="give" pos="vvi" xml:id="A66252-001-a-1580">give</w>
     <w lemma="notice" pos="n1" xml:id="A66252-001-a-1590">Notice</w>
     <w lemma="thereof" pos="av" xml:id="A66252-001-a-1600">thereof</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-1610">to</w>
     <w lemma="we" pos="pno" xml:id="A66252-001-a-1620">Us</w>
     <pc xml:id="A66252-001-a-1630">,</pc>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-1640">or</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-1650">Our</w>
     <w lemma="council" pos="n1" xml:id="A66252-001-a-1660">Council</w>
     <pc xml:id="A66252-001-a-1670">:</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-1680">And</w>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-1690">We</w>
     <w lemma="do" pos="vvb" xml:id="A66252-001-a-1700">do</w>
     <w lemma="promise" pos="n1" xml:id="A66252-001-a-1710">promise</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-1720">to</w>
     <w lemma="he" pos="pno" xml:id="A66252-001-a-1730">him</w>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-1740">or</w>
     <w lemma="they" pos="pno" xml:id="A66252-001-a-1750">them</w>
     <pc xml:id="A66252-001-a-1760">,</pc>
     <w lemma="that" pos="cs" xml:id="A66252-001-a-1770">that</w>
     <w lemma="shall" pos="vmb" xml:id="A66252-001-a-1780">shall</w>
     <w lemma="discover" pos="vvi" xml:id="A66252-001-a-1790">Discover</w>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-1800">or</w>
     <w lemma="apprehend" pos="vvb" xml:id="A66252-001-a-1810">Apprehend</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1820">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66252-001-a-1830">said</w>
     <hi xml:id="A66252-e80">
      <w lemma="Edmond" pos="nn1" xml:id="A66252-001-a-1840">Edmond</w>
      <w lemma="Ludlow" pos="nn1" xml:id="A66252-001-a-1850">Ludlow</w>
     </hi>
     <w lemma="for" pos="acp" xml:id="A66252-001-a-1860">for</w>
     <w lemma="a" pos="d" xml:id="A66252-001-a-1870">a</w>
     <w lemma="reward" pos="n1" xml:id="A66252-001-a-1880">Reward</w>
     <pc xml:id="A66252-001-a-1890">,</pc>
     <w lemma="the" pos="d" xml:id="A66252-001-a-1900">the</w>
     <w lemma="sum" pos="n1" xml:id="A66252-001-a-1910">Sum</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-1920">of</w>
     <w lemma="two" pos="crd" xml:id="A66252-001-a-1930">Two</w>
     <w lemma="hundred" pos="crd" xml:id="A66252-001-a-1940">Hundred</w>
     <w lemma="pound" pos="n2" xml:id="A66252-001-a-1950">Pounds</w>
     <pc unit="sentence" xml:id="A66252-001-a-1960">.</pc>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-1970">And</w>
     <w lemma="we" pos="pns" xml:id="A66252-001-a-1980">We</w>
     <w lemma="do" pos="vvb" xml:id="A66252-001-a-1990">do</w>
     <w lemma="also" pos="av" xml:id="A66252-001-a-2000">also</w>
     <w lemma="hereby" pos="av" xml:id="A66252-001-a-2010">hereby</w>
     <w lemma="give" pos="vvi" xml:id="A66252-001-a-2020">give</w>
     <w lemma="notice" pos="n1" xml:id="A66252-001-a-2030">Notice</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-2040">to</w>
     <w lemma="all" pos="d" xml:id="A66252-001-a-2050">all</w>
     <w lemma="person" pos="n2" xml:id="A66252-001-a-2060">Persons</w>
     <w lemma="that" pos="cs" xml:id="A66252-001-a-2070">that</w>
     <w lemma="shall" pos="vmb" xml:id="A66252-001-a-2080">shall</w>
     <w lemma="conceal" pos="vvb" xml:id="A66252-001-a-2090">Conceal</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2100">the</w>
     <w lemma="say" pos="j-vn" xml:id="A66252-001-a-2110">said</w>
     <hi xml:id="A66252-e90">
      <w lemma="Edmond" pos="nn1" xml:id="A66252-001-a-2120">Edmond</w>
      <w lemma="Ludlow" pos="nn1" xml:id="A66252-001-a-2130">Ludlow</w>
      <pc xml:id="A66252-001-a-2140">,</pc>
     </hi>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-2150">or</w>
     <w lemma="be" pos="vvi" xml:id="A66252-001-a-2160">be</w>
     <w lemma="aid" pos="vvg" xml:id="A66252-001-a-2170">Aiding</w>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-2180">or</w>
     <w lemma="assist" pos="vvg" xml:id="A66252-001-a-2190">Assisting</w>
     <w lemma="in" pos="acp" xml:id="A66252-001-a-2200">in</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2210">the</w>
     <w lemma="conceal" pos="vvg" xml:id="A66252-001-a-2220">Concealing</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-2230">of</w>
     <w lemma="he" pos="pno" xml:id="A66252-001-a-2240">him</w>
     <pc xml:id="A66252-001-a-2250">,</pc>
     <w lemma="or" pos="cc" xml:id="A66252-001-a-2260">or</w>
     <w lemma="further" pos="j-vg" xml:id="A66252-001-a-2270">furthering</w>
     <w lemma="his" pos="po" xml:id="A66252-001-a-2280">his</w>
     <w lemma="escape" pos="n1" xml:id="A66252-001-a-2290">Escape</w>
     <pc xml:id="A66252-001-a-2300">,</pc>
     <w lemma="that" pos="cs" xml:id="A66252-001-a-2310">that</w>
     <w lemma="they" pos="pns" xml:id="A66252-001-a-2320">they</w>
     <w lemma="shall" pos="vmb" xml:id="A66252-001-a-2330">shall</w>
     <w lemma="be" pos="vvi" xml:id="A66252-001-a-2340">be</w>
     <w lemma="proceed" pos="vvn" xml:id="A66252-001-a-2350">Proceeded</w>
     <w lemma="against" pos="acp" xml:id="A66252-001-a-2360">against</w>
     <w lemma="for" pos="acp" xml:id="A66252-001-a-2370">for</w>
     <w lemma="such" pos="d" xml:id="A66252-001-a-2380">such</w>
     <w lemma="their" pos="po" xml:id="A66252-001-a-2390">their</w>
     <w lemma="offence" pos="n1" xml:id="A66252-001-a-2400">Offence</w>
     <pc xml:id="A66252-001-a-2410">,</pc>
     <w lemma="with" pos="acp" xml:id="A66252-001-a-2420">with</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2430">the</w>
     <w lemma="utmost" pos="j" xml:id="A66252-001-a-2440">utmost</w>
     <w lemma="severity" pos="n1" xml:id="A66252-001-a-2450">Severity</w>
     <w lemma="according" pos="j" xml:id="A66252-001-a-2460">according</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-2470">to</w>
     <w lemma="law." pos="ab" xml:id="A66252-001-a-2480">Law.</w>
     <pc unit="sentence" xml:id="A66252-001-a-2490"/>
    </p>
    <p xml:id="A66252-e100">
     <w lemma="give" pos="vvn" xml:id="A66252-001-a-2500">Given</w>
     <w lemma="at" pos="acp" xml:id="A66252-001-a-2510">at</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-2520">Our</w>
     <w lemma="court" pos="n1" xml:id="A66252-001-a-2530">Court</w>
     <w lemma="at" pos="acp" xml:id="A66252-001-a-2540">at</w>
     <hi xml:id="A66252-e110">
      <w lemma="Whitehall" pos="nn1" xml:id="A66252-001-a-2550">Whitehall</w>
     </hi>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2560">the</w>
     <w lemma="fourteen" pos="ord" xml:id="A66252-001-a-2570">Fourteenth</w>
     <w lemma="day" pos="n1" xml:id="A66252-001-a-2580">Day</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-2590">of</w>
     <hi xml:id="A66252-e120">
      <w lemma="November" pos="nn1" xml:id="A66252-001-a-2600">November</w>
      <pc xml:id="A66252-001-a-2610">,</pc>
     </hi>
     <w lemma="1689." pos="crd" xml:id="A66252-001-a-2620">1689.</w>
     <pc unit="sentence" xml:id="A66252-001-a-2630"/>
     <w lemma="in" pos="acp" xml:id="A66252-001-a-2640">In</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2650">the</w>
     <w lemma="first" pos="ord" xml:id="A66252-001-a-2660">First</w>
     <w lemma="year" pos="n1" xml:id="A66252-001-a-2670">Year</w>
     <w lemma="of" pos="acp" xml:id="A66252-001-a-2680">of</w>
     <w lemma="our" pos="po" xml:id="A66252-001-a-2690">Our</w>
     <w lemma="reign" pos="n1" xml:id="A66252-001-a-2700">Reign</w>
     <pc unit="sentence" xml:id="A66252-001-a-2710">.</pc>
    </p>
    <p xml:id="A66252-e130">
     <w lemma="God" pos="nn1" xml:id="A66252-001-a-2720">God</w>
     <w lemma="save" pos="vvb" xml:id="A66252-001-a-2730">save</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2740">the</w>
     <w lemma="king" pos="n1" xml:id="A66252-001-a-2750">King</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-2760">and</w>
     <w lemma="queen" pos="n1" xml:id="A66252-001-a-2770">Queen</w>
     <pc unit="sentence" xml:id="A66252-001-a-2780">.</pc>
    </p>
   </div>
   <div type="colophon" xml:id="A66252-e140">
    <p xml:id="A66252-e150">
     <hi xml:id="A66252-e160">
      <w lemma="LONDON" pos="nn1" xml:id="A66252-001-a-2790">LONDON</w>
      <pc xml:id="A66252-001-a-2800">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A66252-001-a-2810">Printed</w>
     <w lemma="by" pos="acp" xml:id="A66252-001-a-2820">by</w>
     <hi xml:id="A66252-e170">
      <w lemma="Charles" pos="nn1" xml:id="A66252-001-a-2830">Charles</w>
      <w lemma="bill" pos="n1" xml:id="A66252-001-a-2840">Bill</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-2850">and</w>
     <hi xml:id="A66252-e180">
      <w lemma="Thomas" pos="nn1" xml:id="A66252-001-a-2860">Thomas</w>
      <w lemma="newcomb" pos="nn1" xml:id="A66252-001-a-2870">Newcomb</w>
      <pc xml:id="A66252-001-a-2880">,</pc>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A66252-001-a-2890">Printers</w>
     <w lemma="to" pos="acp" xml:id="A66252-001-a-2900">to</w>
     <w lemma="the" pos="d" xml:id="A66252-001-a-2910">the</w>
     <w lemma="king" pos="n1" xml:id="A66252-001-a-2920">King</w>
     <w lemma="and" pos="cc" xml:id="A66252-001-a-2930">and</w>
     <w lemma="queen" pos="n2" xml:id="A66252-001-a-2940">Queens</w>
     <w lemma="most" pos="avs-d" xml:id="A66252-001-a-2950">most</w>
     <w lemma="excellent" pos="j" xml:id="A66252-001-a-2960">Excellent</w>
     <w lemma="majesty" pos="n2" xml:id="A66252-001-a-2970">Majesties</w>
     <pc unit="sentence" xml:id="A66252-001-a-2980">.</pc>
     <w lemma="1689." pos="crd" xml:id="A66252-001-a-2990">1689.</w>
     <pc unit="sentence" xml:id="A66252-001-a-3000"/>
    </p>
   </div>
  </body>
 </text>
</TEI>
